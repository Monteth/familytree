package pl.ct8.monteth.FamilyTree_Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FamilyTreeSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(FamilyTreeSpringApplication.class, args);
	}
}
